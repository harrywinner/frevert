#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pwd.h>
#include <time.h>
#include <limits.h>
#include <libgen.h>
#include <getopt.h>

#define MAX_PATH 256

int is_crontab_installed() {
    if (system("which crontab > /dev/null") == 0) {
        return 1;
    }
    return 0;
}

void create_frevert_directory(char *frevert_dir_path) {
    struct passwd *pw = getpwuid(getuid());
    const char *home_dir = pw->pw_dir;
    snprintf(frevert_dir_path, MAX_PATH, "%s/.frevert", home_dir);

    struct stat st = {0};
    if (stat(frevert_dir_path, &st) == -1) {
        if (mkdir(frevert_dir_path, 0700) == -1) {
            perror("Failed to create .frevert directory");
            exit(1);
        }
    }
}

int write_job_to_file(char *frevert_dir_path, char *abs_file_path) {
    char jobs_file_path[MAX_PATH];
    snprintf(jobs_file_path, MAX_PATH, "%s/jobs", frevert_dir_path);

    FILE *jobs_file = fopen(jobs_file_path, "rw");
    if (jobs_file == NULL) {
        perror("Failed to open jobs file");
        exit(1);
    }

    char line[MAX_PATH];
    while (fgets(line, sizeof(line), jobs_file)) {
        line[strcspn(line, "\n")] = 0;
        // Check if the line (file path) matches the one we want to add
        if (strcmp(line, abs_file_path) == 0) {
            fclose(jobs_file);
            return 1; // Path already exists, return 1
        }
    }

    fprintf(jobs_file, "%s\n", abs_file_path);
    fclose(jobs_file);

    return 0; // Path added to the jobs file
}

void remove_existing_cron_jobs(char *backup_file_path, char *abs_file_path) {
    char crontab_command[512];
    snprintf(crontab_command, sizeof(crontab_command), "crontab -l | grep -v 'cp \"%s\" \"%s\"' | crontab -", backup_file_path, abs_file_path);

    if (system(crontab_command) == -1) {
        perror("Failed to remove existing cron jobs");
    }
}

int copy_file(char *src, char *dest) {
    FILE *fptr1, *fptr2; 
    char c;

    // Open files 
    fptr1 = fopen(src, "r"); 
    if (fptr1 == NULL) 
    { 
        perror("Failed to open source file");
        return 1;
    }
    fptr2 = fopen(dest, "w"); 
    if (fptr2 == NULL) 
    { 
        perror("Failed to open destination file");
        fclose(fptr1);
        return 1;
    } 
  
    // Read contents from file 
    c = fgetc(fptr1); 
    while (c != EOF) 
    { 
        fputc(c, fptr2); 
        c = fgetc(fptr1); 
    }

    // Close files
    fclose(fptr1); 
    fclose(fptr2); 
    return 0; 
}

int restore_file(char *abs_file_path, char *backup_file_path) {
    int copy_err;
    if ((copy_err = copy_file(backup_file_path, abs_file_path)) != 0) {
        perror("Failed to restore file");
        return copy_err;
    }
    printf("File restored from backup.\n");
    return 0;
}

int backup_file(char *abs_file_path, char *backup_file_path) {
    char *backup_dir = strdup(backup_file_path);
    char *backup_dirname = dirname(backup_dir);

    // Use mkdir -p to create directory trees
    char mkdir_command[MAX_PATH];
    snprintf(mkdir_command, MAX_PATH, "mkdir -p \"%s\"", backup_dirname);
    if (system(mkdir_command) == -1) {
        perror("Failed to create backup directory");
        return 1;
    }

    // Copy the file to backup
    int copy_err;
    if ((copy_err = copy_file(abs_file_path, backup_file_path)) != 0) {
        perror("Failed to backup file");
        return copy_err;
    }
    printf("File backup successful.\n");
    return 0;
}

int main(int argc, char *argv[]) {
    if (!is_crontab_installed()) {
        fprintf(stderr, "Crontab is not installed on this system. Cannot schedule cron jobs.\n");
        exit(1);
    }

    int opt;
    int restore_mode = 0;
    char *time_string = NULL;

    while ((opt = getopt(argc, argv, "hvrt:")) != -1) {
        switch (opt) {
            case 'h':
                printf("Usage: %s file_path [-t time_string] [-r]\n", argv[0]);
                printf("-t time_string: Schedule a backup with the provided time string.\n");
                printf("-r: Restore the file from the backup.\n");
                exit(0);
                break;
            case 'v':
                printf("frevert version 1.1\n");
                exit(0);
                break;
            case 'r':
                restore_mode = 1;
                if (time_string) {
                    fprintf(stderr, "Options -r and -t cannot be used together.\n");
                    exit(1);
                }
                break;
            case 't':
                time_string = optarg;
                if (restore_mode == 1) {
                    fprintf(stderr, "Options -r and -t cannot be used together.\n");
                    exit(1);
                }
                break;
            case '?':
                if (optopt != 't') {
                    fprintf(stderr, "Unknown option -%c.\n", optopt);
                }
            default:
                exit(1);
        }
    }

    if (argc - optind < 1) {
        fprintf(stderr, "Usage: %s file_path [[-t time_string] | [-r]]\n", argv[0]);
        exit(1);
    }

    char *file_path = argv[optind];
    char abs_file_path[PATH_MAX];

    if (realpath(file_path, abs_file_path) == NULL) {
        perror("Failed to get absolute path for file");
        exit(1);
    }

    if (optind + 1 < argc && restore_mode) {
        fprintf(stderr, "Error: Cannot use -r option with other options.\n");
        exit(1);
    }

    char frevert_dir_path[MAX_PATH];
    create_frevert_directory(frevert_dir_path);
    char backup_file_path[MAX_PATH];
    snprintf(backup_file_path, MAX_PATH, "%s%s", frevert_dir_path, abs_file_path);

    if (restore_mode) {
        return restore_file(abs_file_path, backup_file_path);
    }

    if (access(abs_file_path, F_OK) == -1) {
        fprintf(stderr, "Error: The file does not exist.\n");
        exit(1);
    }

    backup_file(abs_file_path, backup_file_path);

    // Remove existing cron jobs for the same file
    int existing_jobs = write_job_to_file(frevert_dir_path, abs_file_path);
    if (existing_jobs) {
        remove_existing_cron_jobs(backup_file_path, abs_file_path);
    }

    // Design scheduled job
    char cron_command[256];
    if (time_string) {
        snprintf(cron_command, sizeof(cron_command), "%s cp \"%s\" \"%s\"", time_string, backup_file_path, abs_file_path);
    } else {
        time_t now = time(NULL);
        time_t future_time = now + 24 * 60 * 60; // 24 hours in the future
        struct tm *tm_info = localtime(&future_time);
        char cron_time_string[32];
        strftime(cron_time_string, sizeof(cron_time_string), "%M %H %d %m *", tm_info);
        snprintf(cron_command, sizeof(cron_command), "%s cp \"%s\" \"%s\"", cron_time_string, backup_file_path, abs_file_path);
    }

    // Schedule the job using crontab
    char crontab_command[256];
    snprintf(crontab_command, sizeof(crontab_command), "crontab -l | { cat; echo \'%s\'; } | crontab -", cron_command);
    if (system(crontab_command) == -1) {
        perror("Failed to schedule the cron job");
        exit(1);
    }

    printf("Cron job scheduled.\n");

    return 0;
}
