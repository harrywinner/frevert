Frevert - File Reversion Utility

Frevert - File Reversion Utility
================================

![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)

**Frevert** is a command-line utility designed for system administrators and users who need to manage temporary changes to files while ensuring a safe and automated return to their original state. It's currently in active development with the aim of being released as a Linux and macOS package.

*   **Author:** Harry Winner
*   **Contact:** [harrywinner@icloud.com](mailto:harrywinner@icloud.com)

Table of Contents
-----------------

*   [Installation](#installation)
*   [Usage](#usage)
*   [Features](#features)
*   [Benefits](#benefits)
*   [Contributing](#contributing)
*   [License](#license)

Installation
------------

To get started with Frevert, you'll need to compile it from the source code provided in this repository. Follow these steps to build and install Frevert:

1.  **Clone the Repository:**

    git clone https://gitlab.com/harrywinner/frevert.git
      

2.  **Build and Install Frevert:**

Change your current directory to the project folder and install the executable:

    cd frevert
      make
      export PATH=$PATH:$(pwd)
      

9.  **Verify Installation:**

Verify that Frevert is installed correctly by running:

    cd ~
    frevert -v
      

Usage
-----

Frevert is designed for users who need to manage temporary changes to files while ensuring a safe and automated return to their original state. Here are some common use cases:

### Backup a File

You can use the `frevert` utility to back up a file and have it automatically restored to its current state. The restoration can occur either 24 hours after the backup or at a specific time of your choice using the `-t` option.

*   **Backup a File Without Scheduling**

To create a backup of a file without specifying a restoration time, use the following command:

    frevert /path/to/file
      

By running this command, Frevert will create a backup of the specified file. The restoration will happen automatically 24 hours after the backup. This provides a safety net for your data while ensuring a convenient way to revert changes.

*   **Backup a File with Scheduled Restoration**

If you want more control over when the file should be restored, you can use the `-t` option to specify a restoration time. For example, to schedule a backup with a custom restoration time, use:

    frevert -t "0 0 * * *" /path/to/file
      

In this case, the file will be backed up, and it will automatically be restored at the time specified in the cron schedule. You can customize the schedule to meet your specific needs, allowing for timely and automated restoration of files.

Remember, the `-t` option offers you the flexibility to choose when your file should be reverted, providing a convenient and precise control over the restoration process.

For additional information and usage options, run:

    frevert -h
    

Features
--------

*   **File Backup:** Create backups of files for safekeeping.
*   **Automated Reversion:** Schedule backups to automatically revert changes at specified times.
*   **Listing Backups:** Easily view the list of files with scheduled backups.
*   **Restore:** Quickly restore files to their original state.
*   **Cross-Platform:** Frevert is actively being developed for Linux and macOS, making it a versatile tool for users.

Benefits
--------

### Security Benefits

*   **Data Integrity:** Frevert ensures that critical files and data remain intact. In scenarios where you need to experiment or make temporary changes, the tool acts as a safety net, allowing you to roll back to a known good state.
*   **Scheduled Backups:** With the ability to schedule backups, Frevert adds a layer of automation to data security. You can rest easy knowing that your files will revert to their original state at specified times, reducing the risk of human error.
*   **Protection Against Malicious Changes:** Frevert can help protect your system against accidental or malicious file alterations. By reverting changes automatically, it can mitigate the impact of unauthorized or erroneous modifications.

### Convenience Benefits

*   **User-Friendly Interface:** Frevert offers a simple and user-friendly command-line interface, making it accessible to both novice and experienced users.
*   **File Type Agnostic:** While commonly used for configuration files, Frevert is not limited to any specific file type. It can be used with any files, offering flexibility in managing data.
*   **Transparent and Predictable:** Frevert provides a clear and predictable way to manage file changes. It's easy to understand and work with, enhancing your overall system management experience.
*   **Customization:** Users can customize the scheduling of backups, providing the flexibility to adapt Frevert to their specific needs.

Contributing
------------

Frevert is an open-source project, and contributions are welcome. If you'd like to contribute to the project's development, report issues, or suggest improvements, please contact the author at [harrywinner@icloud.com](mailto:harrywinner@icloud.com).

To contribute code, create a fork of this repository, make your changes, and submit a pull request. Please ensure that your code follows the project's coding standards and is well-documented.

License
-------

Frevert is licensed under the MIT License - see the [LICENSE](LICENSE) file for details. This means that you are free to use, modify, and distribute the software, but you must include the original license text when you distribute it.